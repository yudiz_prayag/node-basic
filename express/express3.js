// router method and router path and Route parameters , query string parameters

const express = require("express");

const app = express();

//use methdo => this methode use as middelware but every request
// app.use((req, res, next) => {
//   console.log("hi");
//   next();
// });

//get methode

app.get("/", (req, res) => {
//   res.redirect("/home");
  res.send("this is root path");
});

//get methode home path
app.get("/home", (req, res) => {
  res.send("this is home path");
});

// then similar post, put, delete methode is work

//all method ==> this methode for all type of http request
//               like get,post,put,delete but when is mach then and then handaler funcation is call
app.all("/prayag", (req, res, next) => {
  res.send("this path is mach or all type of http request");
  // next() // this next funcation is because this over middelware function
});

//we also pass over on function in middelware
//and this function we add as all request, for that we add in app.use methode we pass this function
let test = (req, res, next) => {
  console.log("test is call");
  next();
};
app.get("/test", test, (req, res) => {
  res.send("after test function");
});

app.get("/test2", test, (req, res) => {
  res.send("this is test2 path after test function");
});

//path router
//Route paths can be strings, string patterns, or regular expressions. or array

//normal path
app.get("/normal", (req, res) => {
  res.send("this is noraml path");
});

app.get("/file.txt", (req, res) => {
  res.send("file.txt path");
});

//array as path
//this will be call any of path in array
app.get(["/arr1", "/arr2"], (req, res) => {
  res.send("this is array as path");
});

//string patterns path
app.get("/ab?c", (req, res) => {
  //in this path (ac) is compulsory but b is opation
  res.send("in this abc and ac pathz");
});
app.get("/pq+r", (req, res) => {
  res.send("pqr and pqqr and pqqqr this will call");
});
app.get("/p*r", (req, res) => {
  res.send("start with p and end with r between any string is true");
});

//and more regular expresion is set as path

//Route parameters
app.get("/users/:userId/books/:bookId", (req, res) => {
  // console.log(req.params);
  res.send(req.params);
  
});

// we use (-) and (.) in over prameter
//useing this we tack maltipal value
app.get("/user/:name-:id-:number", (req, res) => {
  res.send(req.params);
});
app.get("/user/:name.:id.:number", (req, res) => {
  res.send(req.params);
});


//quesry string 
//url like this localhost:5000/query?name=value&id=value

app.get('/query',(req,res)=>{
  res.send(req.query);
})

app.listen(5000, () => {
  console.log("sever port 5000 this start");
});
