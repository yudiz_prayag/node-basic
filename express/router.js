const express = require("express");
const data = require("./data");
const fs = require("fs");
const app = express();

// const fList = fs.readFileSync("./data.js", "utf-8");
app.get("/", (req, res) => {
  res.send(`<h1> hello prayag </h1> <a href='/api/name'> check list</a>`);
});
app.get("/api/name", (req, res) => {
  let newList = data.list.map((e) => {
    const { name, age } = e;
    return {name ,age}

  });
  res.json(newList);
});

app.get('/api/name/:nameId',(req,res) => {
  const Id = req.params;
  let singalName = data.list.find(e => {
    return e.id === parseInt(Id.nameId)
  })
  if(!singalName){
    return res.status(404).send('This id is not found')
  }
  res.json(singalName)
})

app.get('/api/v1/query',(req,res)=>{
  console.log(req.query);
  const{search,limit} = req.query
  let testData = [...data.list]
  if (search) {
    testData = testData.filter(fdata => {
      // let a = fdata.name.startsWith(search)
      // console.log('hi',a);
      return fdata.name.startsWith(search)
    })
  }
  if (limit) {
    testData = testData.slice(0,limit)
  }
  if (testData < 1) {
    return res.status(200).json({sucess:true,Data:[]})
  }
  res.send(testData)
})
app.listen(5000, () => {
  console.log("start 5000 now");
});
