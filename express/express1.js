const express = require("express");
//express init
const app = express();

// get methode, home router
app.get('/',(req,res)=>{
    res.send('Home pages')
})

//get methode, api/v1 router
app.get('/api/v1',(req,res)=>{
    res.send('api v1 is now working')
})

//all methode, 404 page not found router
app.all('*',(req,res)=>{
    res.send(`
        <h1> bad request </h1>
        <span> please check you request url </span> <a href='/'> go back</a>
    `)
})

//start server 
app.listen(5000,()=>{
    console.log('start 5000.');
})

// app.get      -> this for get data 
// app.post     -> thi is for add data 
// app.put      -> this is for update data
// app.delete   -> thi is for delete data 
// app.all      -> this is for all http request for example not mach request any router then do any thing with app.all()
// app.use      -> this is for middleware
// app.listen   -> this for port