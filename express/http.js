const http = require("http");
const fs = require('fs');

const homePage = fs.readFileSync('./index.html')
const server = http
  .createServer((req, res) => {
    let url = req.url;
    // console.log("use you side");

    if (url === "/" || url ==='/home') {
      res.writeHead(200, { "content-type": "text/json" });
      res.write(homePage);
      res.end();
    } else if (url === "/about") {
      res.writeHead(200, { "content-type": "text" });
      res.write("about");
      res.end();
    }else{
      
      res.writeHead(404,{'content-type':'text'})
      res.write("go end check");
      res.end();
    }
  })
  .listen(5000, () => {
    console.log("server start");
  });
