const express = require("express");
const crypto = require("crypto");
const db = require("./testDb");
const app = express();

app.use(express.static("./public"));
app.use(express.urlencoded({ extended: true }));

app.post("/register", (req, res) => {
    let { username, password } = req.body;
    let md5 = crypto.createHash("md5");
    let hashp = md5.update(password).digest("hex");
    db.myDb.push({ username, password: hashp });
    res.redirect("/home");
});
app.get("/home", (req, res) => {
    // console.log(req.body);
    res.send("<h1>Welcome</h1>");
});
app.get("/login", (req, res) => {
    // console.log(req.query);
    let { username, password } = req.query;
    let md5 = crypto.createHash("md5");
    let hashp = md5.update(password).digest("hex");
    let check = db.myDb.some(ele => {
        return  (ele.password == hashp && ele.username == username)
     })
     console.log(check);
     if(!check){
         res.send('<h1> you are not valied </h1>')
     }else{
         res.redirect('/home')
     }
});
app.get("/logdb", (req, res) => {
    res.json(db.myDb);
});
app.listen(5000, () => {
    console.log("port 5000 is start");
});
