const nameOfFriends = [
  {
    id: 1,
    name: "prayag",
    age: 21,
    clg: "Rk",
    number: 121223,
  },
  {
    id: 2,
    name: "jeel",
    age: 21,
    clg: "Rk",
    number: 121223,
  },
  {
    id: 3,
    name: "dipak",
    age: 21,
    clg: "Rk",
    number: 121223,
  },
  {
    id: 4,
    name: "akash",
    age: 21,
    clg: "Rk",
    number: 121223,
  },
  {
    id: 5,
    name: "kaushik",
    age: 21,
    clg: "Rk",
    number: 121223,
  },
  {
    id: 6,
    name: "akhatar",
    age: 21,
    clg: "Rk",
    number: 121223,
  },
];

const nameId = [
  { name: "prayag", id: 1 },
  { name: "akash", id: 2 },
  { name: "dipak", id: 3 },
  { name: "nayan", id: 4 },
  { name: "jeel", id: 5 },
];

module.exports = {nameOfFriends,nameId};
